module bitbucket.org/pcas/hpc

go 1.13

require (
	bitbucket.org/pcas/metrics v0.1.35
	bitbucket.org/pcastools/convert v1.0.5
)
