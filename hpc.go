// Hpc sets up logging and metrics in a manner suited for running on PBS-managed HPC systems.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package hpc

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/convert"
	"os"
)

// Global variables that record data about the HPC system on which we
// are running
var (
	Hostname string // the hostname on which we are running
	JobName  string // the job name on the HPC system
	JobID    int    // the job ID on the HPC system
	ArrayID  int    // the array ID on the HPC system
	PID      int    // the PID of the job
	Username string // the username of the user running the job
)

// init initialises global variables and sets global tags for metrics
// collection
func init() {
	// hostname
	Hostname, _ = os.Hostname()
	metrics.SetGlobalTag("hpc.Hostname", Hostname)
	// job name
	JobName = os.Getenv("PBS_JOBNAME")
	metrics.SetGlobalTag("hpc.JobName", JobName)
	// job ID
	var x string
	x = os.Getenv("PBS_JOBID")
	JobID, _ = convert.ToInt(x)
	metrics.SetGlobalTag("hpc.JobID", x)
	// array ID
	x = os.Getenv("PBS_ARRAYID")
	ArrayID, _ = convert.ToInt(x)
	metrics.SetGlobalTag("hpc.ArrayID", x)
	// PID
	PID = os.Getpid()
	x, _ = convert.ToString(PID)
	metrics.SetGlobalTag("hpc.PID", x)
	// username
	Username = os.Getenv("USER")
	metrics.SetGlobalTag("hpc.Username", Username)
}
